const gameContainer = document.getElementById("game");


const gifdata = [
    "1.gif",
    "2.gif",
    "3.gif",
    "4.gif",
    "5.gif",
    "6.gif",
    "7.gif",
    "8.gif",
    "9.gif",
    "10.gif",
    "11.gif",
    "12.gif",
    "1.gif",
    "2.gif",
    "3.gif",
    "4.gif",
    "5.gif",
    "6.gif",
    "7.gif",
    "8.gif",
    "9.gif",
    "10.gif",
    "11.gif",
    "12.gif",
];


function shuffle(array) {
  let counter = array.length;

  while (counter > 0) {

    let index = Math.floor(Math.random() * counter);
    counter--;
    
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  
  }

  return array;
}


function createDivsForColors(colorArray) {

  for (let color of colorArray) {

    const newDiv = document.createElement("div");
    newDiv.classList.add("color");

    const backgif = document.createElement("img");
    const frontimages = document.createElement("img");

    frontimages.src="jokers.png"
    frontimages.classList.add("front-face");

    backgif.src="./gifs/"+color;
    backgif.classList.add("back-face");


    newDiv.append(backgif);
    newDiv.append(frontimages);
    gameContainer.append(newDiv);

  }

  localStorageValue();

}

createDivsForColors(shuffle(gifdata));


function localStorageValue(){
    const BestTimeValue = document.getElementById("highestTime");

    if(!localStorage.getItem("MemoryGame")){
    
      let initialarray = [{'name':"NONE",'time':"00:00"}];
      localStorage.setItem("MemoryGame" ,JSON.stringify(initialarray));
    
    } else {

      let memory=JSON.parse(localStorage.getItem("MemoryGame"));
      BestTimeValue.innerText = `${memory[0].name}:${memory[0].time}`;
      document.getElementById("highScore-value").innerText = memory[0].time;
    
    }
}


main( document.querySelectorAll("#game div") );


function main( cardList ){

  let matchcount=0;
  let trail=0;
  let hasfirstCard = false;
  let frezing = false;
  let firstCard , secondCard;
  
  cardList.forEach(( card )=>{

    card.addEventListener( "click" , handleCardClick);
  
  });


  function handleCardClick(event) {

    if( ! document.getElementById("input-bar").value){
      alert("Please Enter Your Name!");
    }

    if(!frezing){ 
    
      event.target.parentElement.classList.toggle('flip');

      if(trail === 0){

        console.log("inside");
        timer();
      
      }

      if(!hasfirstCard){
        trail++;
        hasfirstCard = true;
        firstCard = event.target.parentElement;

      } else {
        
        hasfirstCard = false;
        secondCard = event.target.parentElement;

        checkingForMatches(firstCard ,secondCard);
      }
    }

  }


  function checkingForMatches(firstCard ,secondCard){

    if( firstCard.classList.value+"" === "color flip"  && secondCard.classList.value+"" === "color flip"){
            
      if(firstCard.children[0].src+""  === secondCard.children[0].src){
        
        //Disabling a cards
        firstCard.removeEventListener("click" , handleCardClick);
        secondCard.removeEventListener("click" , handleCardClick);
        matchcount++;

        if(matchcount === 12){

          clearInterval(gameTimer);
          let string =document.getElementById("timer").innerText;
          BestTimer(string.slice(6,11));

        }
  
        } else {
          frezing = true;
  
        setTimeout(()=>{
          
          firstCard.classList.remove("flip");
          secondCard.classList.remove("flip");
          frezing = false;
        
        } ,1500);
      }  
    }
  }
}


function timer(value){
  let sec =0;
  let mins =0;
  let ss="";
  let mm="";

  gameTimer = setInterval(() => {
      
    if(sec === 60){
      sec=0;
      mins++;
    }
    sec++;
    
    if(sec < 10){
      ss = `0${sec}`;
    } else{

      ss =sec;
    }

    if(mins < 10){
      mm =`0${mins}`;
    } else {
      mm = mins;
    }

  document.querySelector('#timer').innerText = `Timer ${mm}:${ss}`;
}, 1000);
 
}


function BestTimer(besttime){

  comparingBestTime(besttime); 
}


function comparingBestTime(besttime){

  let usertime=((parseInt(besttime.slice(0,2))*60)+parseInt(besttime.slice(3,6)));

  let timeInLocal = JSON.parse(localStorage.getItem("MemoryGame"))[0].time;
  let timeInLocalinseconds = ((parseInt(timeInLocal.slice(0,2))*60)+parseInt(timeInLocal.slice(3,5)));

  if(usertime < timeInLocalinseconds || timeInLocalinseconds === 0){
    localStorageManipulation(besttime);
  } 
}


function localStorageManipulation(besttime){

  let username = document.getElementById("input-bar").value;
  let initialarray = [{'name':username,'time':besttime}];
  localStorage.setItem("MemoryGame" ,JSON.stringify(initialarray));
  document.getElementById("highScore-value").innerText = besttime;
  document.getElementById("highestTime").innerText = `${username} : ${besttime}`;  

}
